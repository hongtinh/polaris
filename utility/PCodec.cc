//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <unistd.h>
#include <arpa/inet.h>
#include "PCodec.h"
#include "../utility/PLog.h"

namespace polaris {

const int PSizeOfHeaderLen = sizeof(int32_t);

PCodec::PCodec(std::string delimiter):sliceBuf_(fixedSlice_, PCODEC_SLICE_BUF_SIZE, PCODEC_SLICE_BUF_SIZE)
{
    delimiter_ = delimiter;
    reset();
}

PCodec::~PCodec()
{
}

void PCodec::reset()
{
    sliceBuf_.clear();
    currentPacket_.clear();
    legacySize_ = 0;
}

//encoded structure,
//{ [-delimiter-][-totalSize-][-msgNameLen-][-msgName-][-byteData-] }
void PCodec::encode(const PMsg *msg, std::string *packet)
{
    packet->clear();
    packet->append(delimiter_);
    packet->resize(PSizeOfHeaderLen + delimiter_.size());
    const std::string& typeName = msg->GetTypeName();
    int32_t nameLen = static_cast<int32_t>(typeName.size() + 1);
    int32_t be32 = ::htonl(nameLen);
    packet->append(reinterpret_cast<char*>(&be32), sizeof(be32));
    packet->append(typeName.c_str(), nameLen);

    msg->AppendToString(packet);
    int32_t len = ::htonl(packet->size() - PSizeOfHeaderLen - delimiter_.size());
    std::copy(reinterpret_cast<char*>(&len), reinterpret_cast<char*>(&len) + sizeof(len), packet->begin() + delimiter_.size());
}

PMsg* PCodec::decode(const std::string& packet)
{
    int32_t len = static_cast<int32_t>(packet.size());
    int32_t nameLen = pickbe32toh(packet.c_str());
    std::string typeName = packet.substr(sizeof(nameLen), nameLen - 1);

    PMsg* msg = createMsg(typeName);

    if (msg) {
        const char* data = packet.c_str() + nameLen + sizeof(nameLen);
        int32_t dataLen = len - nameLen - sizeof(nameLen);
        if (msg->ParseFromArray(data, dataLen)) {
            return msg;
        } else {
            PLOG_ERROR("msg %s parse from array failed!!\n", typeName.c_str());
            delete msg;
            return NULL;
        }
    } else {
        PLOG_ERROR("msg %s is unknown!!\n", typeName.c_str());
        return NULL;
    }
}

//google protobuf reflection, import from muduo of Chen Shuo (chenshuo at chenshuo dot com)
PMsg* PCodec::createMsg(const std::string& type_name)
{
    google::protobuf::Message* message = NULL;
    const google::protobuf::Descriptor* descriptor =
        google::protobuf::DescriptorPool::generated_pool()->FindMessageTypeByName(type_name);
    if (descriptor) {
        const google::protobuf::Message* prototype =
            google::protobuf::MessageFactory::generated_factory()->GetPrototype(descriptor);
        if (prototype) {
            message = prototype->New();
        }
    }
    return message;
}


bool PCodec::validatePacketData(const char *packetData)
{
    assert(packetData);
    return delimiter_.compare(packetData) == 0;
}

int PCodec::locateDelimiter(const char*packetData, int packetSize)
{
    assert(packetData);
    std::string pdata = std::string(packetData, packetSize);
    return pdata.find(delimiter_);
}

int32_t PCodec::pickbe32toh(const char *buf)
{
    int32_t be32 = 0;
    memcpy(&be32, buf, sizeof(be32));
    return be32toh(be32);
}

PMsg* PCodec::collectOneMsg(char *data, size_t dataSize)
{
    PMsg* msg = NULL;
    
    if (dataSize < delimiter_.size()) {
        PLOG_ERROR("data is less than delimiter size, data size:%lu, delimiter size:%lu\n", dataSize, delimiter_.size());
        return NULL;
    }
    
    if (dataSize < sizeof(delimiter_.size() + PSizeOfHeaderLen)) {
        PLOG_ERROR("too little data, less than packet header size, data size:%lu, header size:%lu\n", 
            dataSize, sizeof(delimiter_.size() + PSizeOfHeaderLen));
        return NULL;
    } 
    
    if (! validatePacketData(data)) {
        PLOG_ERROR("no delimiter found, dropping packet data.\n"); 
        return NULL;
    }

    PBuffer buf(data, dataSize, dataSize);

    //remove the delimiter.
    buf.reduce(delimiter_.size());
    size_t packetSize = pickbe32toh(buf.data());
    //remove the headerlen.
    buf.reduce(PSizeOfHeaderLen);
    
    std::string packet;
   
    if (buf.size() >= packetSize) {
        packet.append(buf.data(), packetSize);
        msg = decode(packet);
    } else {
        PLOG_ERROR("data is not enough, data size:%lu, expected size:%lu.\n", dataSize, packetSize); 
        return NULL;
    }

    return msg;
    
}

void PCodec::collectMsg(char *data, int dataSize, std::deque<PMsg*> *outQueue)
{
    assert(legacySize_ >= 0);
    assert(data);

    if (!sliceBuf_.empty()) {
        memmove(data + sliceBuf_.size(), data, dataSize);
        memcpy(data, sliceBuf_.data(), sliceBuf_.size());
        dataSize += sliceBuf_.size();
        sliceBuf_.clear();
    }

    PBuffer buf(data, dataSize, dataSize);

    while (buf.size() > 0) {

        if (legacySize_ == 0) {

            if (buf.size() < delimiter_.size()) {

                sliceBuf_ = buf;
                //fprintf(stderr, "data is less than delimiter size, expecting for next read, %d\n", buf.size() );
                return;

            }

            if (! validatePacketData(buf.data())) {
                 int res = locateDelimiter(buf.data(), buf.size());

                if (res < 0) {
                    // we will keep at least PPacketDelimiterSize bytes, for cases like [***********************\r\n\r]
                    // where the delimiter is "\r\n\r\n".
                    buf.reduce(buf.size() - delimiter_.size());
                    sliceBuf_ = buf;
                    PLOG_ERROR("no delimiter found, dropping extra packet data, drop size : %lu\n", buf.size() - delimiter_.size());            
                    return;
                }

                if (res > 0) {
                    buf.reduce(res);
                    PLOG_ERROR("found delimiters, but got some extra packet data, dropping, drop size : %d\n", res);
                    continue;
                }

            }

            if (buf.size() < sizeof(delimiter_.size() + PSizeOfHeaderLen)) {
                //fprintf(stderr, "too little data, less than packet header size, expecting for next read, %d\n", buf.size());
                sliceBuf_ = buf;
                return;
            }

            currentPacket_.clear();
            //remove the delimiter.
            buf.reduce(delimiter_.size());
            size_t packetSize = pickbe32toh(buf.data());
            //remove the headerlen.
            buf.reduce(PSizeOfHeaderLen);


            if (buf.size() >= packetSize) {
                currentPacket_.append(buf.data(), packetSize);
                PMsg* msg = decode(currentPacket_);
                if (msg) {
                    outQueue->push_back(msg);
                }
                currentPacket_.clear();
                buf.reduce(packetSize);        
            } else {
                currentPacket_.append(buf.data(), buf.size());
                legacySize_ = packetSize - buf.size();
                buf.clear();
            }

        }else {

            if (buf.size() < legacySize_) {
                currentPacket_.append(buf.data(), buf.size());
                legacySize_ -= buf.size();
                buf.clear();

            } else {

                currentPacket_.append(buf.data(), legacySize_);
                PMsg* msg = decode(currentPacket_);
                if (msg) {
                    outQueue->push_back(msg);
                }
                currentPacket_.clear();

                buf.reduce(legacySize_);        
                legacySize_ = 0;

            }

        }
    }

}


}

