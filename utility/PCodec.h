//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PCODEC_H__
#define __POLARIS_PCODEC_H__

#include <string>
#include <stdint.h>
#include <deque>
#include "PBuffer.h"
#include "../rpc/PMsg.h"

namespace polaris {

class PCodec{
  
public:
    PCodec(std::string delimiter);
    ~PCodec();
    void encode(const PMsg *msg, std::string *packet);  
    PMsg* decode(const std::string& packet);
    void collectMsg(char *data, int dataSize, std::deque<PMsg*> *outQueue);   
    PMsg* collectOneMsg(char *data, size_t dataSize);
    void reset();
    void dump_bytes(const void *data, int size){
		int i = 0;
		char *p =(char*)data;
		printf("dump_bytes start:\n");
		while( i < size ){
			if( 0 == i%64 ) printf("\n");
			printf("%02x", *(p+i));
			i++;
		}
		printf("\ndump_bytes end:\n");
    }    

    enum {
        //32 Bytes is enough for small partial pieces.
        PCODEC_SLICE_BUF_SIZE = 32
    };
private:
    bool validatePacketData(const char *packetData);
    int locateDelimiter(const char *packetData, int packetSize);
    int32_t pickbe32toh(const char *buf);
    PMsg* createMsg(const std::string& type_name);
  
private:
    std::string delimiter_;
    char fixedSlice_[PCODEC_SLICE_BUF_SIZE];
    PBuffer sliceBuf_;
    std::string currentPacket_;
    size_t legacySize_;
};


}

#endif

