//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PLOG_H__
#define __POLARIS_PLOG_H__

#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <unistd.h>
#include <assert.h>
#include <thread>
#include <sys/types.h> 
#include <sys/syscall.h>
#include "PMutexLock.h"

namespace polaris{

extern thread_local pid_t threadId;

// thread safe
class PLog{
public:
   //No Copying
    PLog(const PLog&) = delete;
    PLog& operator=(const PLog&) = delete;    
    ~PLog() {
        close();
    }

    static PLog* getInstance() {
        pthread_once(&ponce_, &PLog::init);
        assert(instance_ != NULL);
        return instance_;
    }

    static void init() {
        instance_ = new PLog();
    }

    void open(const char *pathName);
    void close();
    void setRotateSize(int size);
    void put(const char *level, const char *format, ...);
    void setLevel(int level);
    int level() { return level_; }
    pid_t threadId() { return threadId_; }


private:
    PLog():
        logFile_(""),
        log_(NULL),
        rotateSize_(1024*1024*10),
        currentSize_(0),
        level_(TRACE),
        lastFlushSecs_(0) {
            threadId_ = gettid();
        };
        
    pid_t gettid() {  
        return syscall(SYS_gettid);  
    }  
    void formatTimeStamp();
    void write(const char *level);
    void rotate();
    int mapLevel(const char *level);

public:

    enum LogLevel {
        TRACE = 0,
        DEBUG,     
        INFO,
        WARN,
        ERROR
    };

private:
    //singleton
    static pthread_once_t ponce_;
    static PLog *instance_;
    pid_t threadId_;

    //log file
    std::string logFile_;
    FILE *log_;
    char logBuf_[1024];

    //log option
    size_t rotateSize_;
    size_t currentSize_;
    int level_;

    //log timestamp
    struct timeval tv_;
    time_t lastFlushSecs_;
    char timeBuf_[64];

    std::mutex mtx_;
};


}

#define PLOG_OPEN(pathName) do { polaris::PLog::getInstance()->open(pathName); }while(0)
#define PLOG_CLOSE() do { polaris::PLog::getInstance()->close(); delete polaris::PLog::getInstance();}while(0)
#define PLOG_WARN(...) do { polaris::PLog::getInstance()->put( "WARN",  __VA_ARGS__); }while(0)
#define PLOG_TRACE(...) do { polaris::PLog::getInstance()->put( "TRACE",  __VA_ARGS__); }while(0)
#define PLOG_DEBUG(...) do { polaris::PLog::getInstance()->put( "DEBUG", __VA_ARGS__); }while(0)
#define PLOG_INFO(...) do { polaris::PLog::getInstance()->put( "INFO",  __VA_ARGS__ ); }while(0)
#define PLOG_ERROR(...) do { polaris::PLog::getInstance()->put("ERROR",  __VA_ARGS__); }while(0)


#endif

