//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PBUFFER_H__
#define __POLARIS_PBUFFER_H__

#include <assert.h>
#include <string.h>

namespace polaris{

class PBuffer{

public:
    PBuffer(char *data, size_t size, size_t cap):
        cap_(cap),
        data_(data),
        size_(size),
        start_(0)
    {
        assert(data);
        assert(cap >= size);
    }

    ~PBuffer() {}

    void clear() { size_ = 0; start_ = 0; }
    bool empty() const { return (size_ == 0); }
    size_t size() const { return size_; }

    char* data() const { 
        assert(start_ <= cap_); 
        return (data_ + start_); 
    }  

    void reduce(size_t size) {
        assert(size <= size_);
        if (size_ == size) {
            clear();
            return;
        }
        size_ -= size;
        start_ += size;
        assert(start_ <= cap_);
    }

    PBuffer& operator=(const PBuffer &buf) {
        assert(cap_ >= buf.size());
        memcpy(data_, buf.data(), buf.size());
        size_ = buf.size();
        start_ = 0;
        return *this;
    }

private:
    size_t cap_;
    char *data_;
    size_t size_;
    size_t start_;
};

}


#endif
