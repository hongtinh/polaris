//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:

//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.

//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <stdarg.h>
#include <sys/types.h>
#include <sys/time.h>
#include <string.h>
#include "PLog.h"

namespace polaris{

PLog *PLog::instance_ = NULL;
pthread_once_t PLog::ponce_ = PTHREAD_ONCE_INIT;
thread_local pid_t threadId = 0;

void PLog::open(const char *pathName)
{
    PMutexLock lock(&mtx_);
    if (log_) {
        fprintf(stderr, "PLog is already opened as %s\n", logFile_.c_str());
        return;
    }

    logFile_ = std::string(pathName);
    char pid[32];
    snprintf(pid, 32, ".%d", (int)::getpid());
    logFile_.append(pid);

    log_ = ::fopen(logFile_.c_str(), "a+");
    if (!log_) {
        perror("PLog open failed");
        return;
    }
}

void PLog::close()
{
    PMutexLock lock(&mtx_);
    if (log_) {
        ::fclose(log_);
        log_ = NULL;
    }
}

void PLog::setRotateSize(int size)
{
    PMutexLock lock(&mtx_);
    rotateSize_ = size;
}

void PLog::put(const char *level, const char *format, ...)
{
    PMutexLock lock(&mtx_);
    if (!log_) {
        fprintf(stderr,"PLog is not opened!!\n");
        return;
    }

    int lv = mapLevel(level);
    if (lv < level_) {
        return;
    }

    va_list argptr;
    va_start(argptr, format);
    vsnprintf(logBuf_, 1024, format, argptr);
    va_end(argptr);

    write(level);
    rotate();
}

void PLog::setLevel(int level)
{
    PMutexLock lock(&mtx_);
    level_ = level;
}

int PLog::mapLevel(const char *level)
{
    if (*level == 'T') {
        return TRACE;
    } else if (*level == 'D') {
        return DEBUG;
    } else if (*level == 'I') {
        return INFO;
    } else if (*level == 'W') {
        return WARN;
    } else if (*level == 'E') {
        return ERROR;
    } else {
        // TRACE by default
        return TRACE;
    }
}

void PLog::formatTimeStamp()
{
    gettimeofday(&tv_, NULL);
    int offset = strftime(timeBuf_, 64, "%Y%m%d %H:%M:%S.", localtime(&tv_.tv_sec));
    snprintf(timeBuf_ + offset, 64 - offset, "%03d", (int)tv_.tv_usec/1000);
}

void PLog::write(const char *level)
{
    formatTimeStamp();
    int size = fprintf(log_, "%s [%d] <%s> %s", timeBuf_, threadId_, level, logBuf_);

    //flush if log interval is bigger than 3 seconds.
    if (difftime(tv_.tv_sec, lastFlushSecs_) > 3.0) {
        ::fflush(log_);
        lastFlushSecs_ = tv_.tv_sec;
    }

    //DEBUG
    if (mapLevel(level) >= level_) {
        fprintf(stdout, "%s [%d] <%s> %s", timeBuf_, threadId_, level, logBuf_);
    }
    
    currentSize_ += size;
    memset(timeBuf_, 0, 64);
    memset(logBuf_, 0, 1024);
}

void PLog::rotate()
{
    if (currentSize_ > rotateSize_) {
        ::fclose(log_);

        std::string oldLogFile = logFile_;
        oldLogFile.append(".old");
        ::rename(logFile_.c_str(), oldLogFile.c_str());

        log_ = ::fopen(logFile_.c_str(), "a+");
        currentSize_ = 0;
    }

}

}
