//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <unistd.h>
#include "PEpoll.h"
#include "PEventOwner.h"


namespace polaris{

PEpoll::PEpoll()
{
    events_.resize(256);
    //1024 is just a hint for the kernel, no used since 2.6.8.
    eFd_ = epoll_create(1024); 
    assert(eFd_ != -1);
}

PEpoll::~PEpoll()
{
    ::close(eFd_);
}

bool PEpoll::addEvent(PEventOwner *owner, int mask)
{
    struct epoll_event ev;
    ev.events = 0;
    ev.data.u64 = 0; //avoid valgrind warning
    //ev.data.fd = fd;
    ev.data.ptr = owner;


    int action = (owner->pollMask()== PEventOwner::PPOLL_NONE) ? EPOLL_CTL_ADD : EPOLL_CTL_MOD;
    
    mask |= owner->pollMask();

    if (mask & PEventOwner::PPOLL_READABLE) {
        ev.events |= EPOLLIN;
    }

    if (mask & PEventOwner::PPOLL_WRITEABLE) {
        ev.events |= EPOLLOUT;
    }

    if (epoll_ctl(eFd_, action, owner->fd(), &ev) == -1) {
        perror("addEvent");
        return false;
    }

    return true;
}

bool PEpoll::delEvent(PEventOwner *owner, int mask)
{
    struct epoll_event ev;
    ev.events = 0;
    ev.data.u64 = 0; //avoid valgrind warning
    ev.data.ptr = owner;

    int currentMask = owner->pollMask();
    currentMask &= (~mask);

    if (currentMask != PEventOwner::PPOLL_NONE) {

        if (currentMask & PEventOwner::PPOLL_READABLE) {
            ev.events |= EPOLLIN;
        }

        if (currentMask & PEventOwner::PPOLL_WRITEABLE) {
            ev.events |= EPOLLOUT;
        }    
        
        if (epoll_ctl(eFd_, EPOLL_CTL_MOD, owner->fd(), &ev) == -1) {
            perror("delEvent");
            return false;
        }
        
    } else {
        //Note, Kernel < 2.6.9 requires a non null event pointer even for EPOLL_CTL_DEL.
        if (epoll_ctl(eFd_, EPOLL_CTL_DEL, owner->fd(), &ev) == -1) {
            perror("delEvent");
            return false;
        }
    }

    return true;
}


int PEpoll::getMask(struct epoll_event *event)
{
    if (event->events & EPOLLIN) return PEventOwner::PPOLL_READABLE;
    if (event->events & EPOLLOUT) return PEventOwner::PPOLL_WRITEABLE;
    if (event->events & EPOLLERR) return PEventOwner::PPOLL_WRITEABLE;
    if (event->events & EPOLLHUP) return PEventOwner::PPOLL_WRITEABLE;
    return PEventOwner::PPOLL_NONE;
}

int PEpoll::wait(int timeout)
{
    size_t eventNum = epoll_wait(eFd_, events_.data(), events_.size(), timeout);
    //double it.
    if (eventNum == events_.size()) {
        events_.resize(eventNum * 2);
    }
    return eventNum;
}


std::vector<struct epoll_event>& PEpoll::getEvents()
{
    return events_;
}


}


