//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <functional>
#include <memory>
#include "PServer.h"
#include "PEventLoop.h"
#include "PSocket.h"
#include "PMsgRouter.h"
#include "PTimer.h"
#include "PChannel.h"
#include "PScreener.h"
#include "../utility/PLog.h"


namespace polaris{

PServer::PServer(PEventLoop *loop)
{
    assert(loop);
    int fd = PSocket::create(AF_INET);
    PSocket::setNonBlock(fd, true);
    PSocket::setReuseAddr(fd, true);
    PSocket::setKeepAlive(fd, true, 30);
    setFd(fd);

    loop_ = loop;
    running_= false;
    msgRouter_ = NULL;
    channelMaxIdleTime_ = 30; // 30 seconds by default.
    screener_ = NULL;
}

PServer::~PServer()
{
    stop();
    if(screener_) delete screener_;
}

bool PServer::listen(int port)
{
    if (running_) {
        PLOG_ERROR("PServer already running !!\n");
        return false;    
    }

    PSocket::listen(fd(), port);
    if (! loop_->addFdEvent(this, PEventOwner::PPOLL_READABLE)) {
        PLOG_ERROR("PServer::listen, add read event failed!!\n");
        return false;
    }

    setupScreener();

    running_ = true;
    PLOG_INFO("PServer starts successfully.\n");
    return true;
}

bool PServer::stop()
{
    if (running_ && (! loop_->delFdEvent(this, PEventOwner::PPOLL_READABLE))) {
        PLOG_ERROR("PServer::stop, remove read event failed!!\n");
        return false;
    }

    PSocket::close(fd());
    running_ = false;
    PLOG_INFO("PServer stops successfully.\n");
    return true;  
}

void PServer::setupScreener()
{
    if (!screener_) {
        screener_ = new PScreener(loop_);
        screener_->start(channelMaxIdleTime_);
        PLOG_INFO("PScreener starts, channelMaxIdleTime:%d seconds.\n", channelMaxIdleTime_);
    }
}

void PServer::accept(int fd)
{
    struct sockaddr_in addr;
    int afd = PSocket::accept(fd, &addr);
    if (afd == -1) {
        PLOG_ERROR("PServer accept :%d fail !!\n", fd);
        perror("accept fail");
        return;
    }

    std::shared_ptr<PChannel> ch(new PChannel(loop_, afd, msgRouter_));
    if (screener_) {
        ch->setScreener(screener_);
        screener_->manage(ch);
    }
}

void PServer::processReadEvent()
{
    accept(fd());
}

void PServer::processWriteEvent()
{
    //do nothing.
}


}



