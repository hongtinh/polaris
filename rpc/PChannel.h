//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PCHANNEL_H__
#define __POLARIS_PCHANNEL_H__

#include <string>
#include <deque>
#include <memory>
#include "PEventOwner.h"
#include "PEventLoop.h"
#include "PMsg.h"
#include "PSocket.h"
#include "../utility/PCodec.h"

namespace polaris{

class PScreener;
class PMsgRouter;

class PChannel : public PEventOwner, public std::enable_shared_from_this<PChannel> {

public:
    PChannel(PEventLoop *loop, int fd, PMsgRouter *router);
    ~PChannel();
    std::shared_ptr<PChannel> getPtr() { return shared_from_this();}
    PEventLoop* loop() { return loop_; }
    int state() { return state_; }  
    void setAutoReconnect(bool on) { autoConnect_ = on; }
    bool isAutoReconnect() { return autoConnect_; }
    bool sendMsg(PMsg *msg);
    void close();  
    void setScreener(PScreener *screener);
    void unScreen();
    bool isScreened() { return (screener_ != NULL); }
    int screenBucketIndex() { return screenBucketIndex_; }
    void setScreenBucketIndex(int index) {screenBucketIndex_ = index; }

private:
    void dispatchMsg();
    void setupSocket(PEventLoop *loop, int fd);  
    void processReadEvent();
    void processWriteEvent();
    void checkin();
    bool reconnect();  
    void closeCleanup();

public:
    enum {
        PCH_DISCONNECTED = 0,        
        PCH_CONNECTED = 1,
    };
  
private:
    PEventLoop *loop_;  
    PMsgRouter* msgRouter_;
    struct sockaddr_in addr_;
    std::string ipport_;  
    std::deque<std::string*> writeQueue_;
    std::deque<PMsg*> readQueue_;
    PCodec codec_;
    int state_;
    bool autoConnect_;
    PScreener *screener_;
    int screenBucketIndex_;
    size_t legacySendingPacketSize_;
};


}

#endif

