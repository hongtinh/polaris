//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PEVENTOWNER_H__
#define __POLARIS_PEVENTOWNER_H__

namespace polaris{

class PEventOwner {
public:
    PEventOwner():
        currentPollMask_(PPOLL_NONE) 
    {}
    virtual ~PEventOwner() {}
    int fd() { return fd_; }
    void setFd(int fd) { fd_ = fd; }    
    virtual void processReadEvent() = 0;
    virtual void processWriteEvent() = 0;
    
    // No copying  
    PEventOwner(const PEventOwner&) = delete;
    PEventOwner& operator=(const PEventOwner&) = delete;
    
    int pollMask() { return currentPollMask_; }
    void updateMask(int mask) { currentPollMask_ = mask; }

public:
    enum PPollMask{
        PPOLL_NONE = 0x00, // 0x00
        PPOLL_READABLE = 0x01, //0x01
        PPOLL_WRITEABLE = 0x02, //0x02
    };
    
private:
    int currentPollMask_;
    int fd_;
};

}

#endif
