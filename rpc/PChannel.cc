//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <errno.h>
#include "PChannel.h"
#include "PMsgRouter.h"
#include "PSocket.h"
#include "PScreener.h"
#include "../utility/PLog.h"

namespace polaris{

PChannel::PChannel(PEventLoop *loop, int fd, PMsgRouter *router):
    loop_(loop),
    msgRouter_(router),
    codec_("\r\n\r\n"),
    state_(PCH_DISCONNECTED),
    autoConnect_(false),
    screener_(NULL),
    screenBucketIndex_(0),
    legacySendingPacketSize_(0)
{
    writeQueue_.clear();
    readQueue_.clear();

    setupSocket(loop, fd);

    if (!loop_->addFdEvent(this, PEventOwner::PPOLL_READABLE)) {
        PLOG_ERROR("Failed to setup PChannel event for %s !!\n", ipport_.c_str());
        state_ = PCH_DISCONNECTED; 
        return;
    }
    state_ = PCH_CONNECTED; 
    PLOG_INFO("PChannel for %s is setup successfully, fd:%d\n", ipport_.c_str(), fd);  
}


PChannel::~PChannel()
{
    close();
}

void PChannel::setupSocket(PEventLoop *loop, int fd)
{
    PSocket::setNonBlock(fd, true);
    PSocket::setKeepAlive(fd, true, 30);

    addr_ = PSocket::getPeerAddr(fd);    
    ipport_ = PSocket::getIpPort(static_cast<struct sockaddr_in*>(&addr_)); 
    setFd(fd);
}

void PChannel::close()
{
    if (PCH_DISCONNECTED == state_) return;
    
    if (!loop_->delFdEvent(this, PEventOwner::PPOLL_READABLE | PEventOwner::PPOLL_WRITEABLE)) {
        PLOG_ERROR("clear events for PChannel:%s fail !!\n", ipport_.c_str());
    }
    PSocket::close(fd());  
    state_ = PCH_DISCONNECTED;
    closeCleanup();
}

void PChannel::closeCleanup()
{
    for (auto packet : writeQueue_) {
        if (packet) delete packet;
    } 
    
    for (auto msg : readQueue_) {
        if (msg) delete msg;
    } 

    codec_.reset();
    legacySendingPacketSize_ = 0;
}


void PChannel::setScreener(PScreener *screener)
{
    screener_ = screener;
    screenBucketIndex_ = -1;
}

void PChannel::unScreen()
{
    screener_ = NULL;
    screenBucketIndex_ = -1;
}

void PChannel::checkin()
{
    if (!screener_) return;
    std::shared_ptr<PChannel> ch = getPtr();
    screener_->arrange(ch);
}

bool PChannel::sendMsg(PMsg *msg)
{
    assert(msg);
    if (state_ != PCH_CONNECTED) {
        if (! reconnect()) {
            PLOG_ERROR("sendMsg fail, connection of %s is not in connected state !!\n", ipport_.c_str());
            return false;
        } 
    }
    
    std::string *packet = new std::string();
    codec_.encode(msg, packet);

    writeQueue_.push_back(packet);

    if (!loop_->addFdEvent(this, PEventOwner::PPOLL_WRITEABLE)) {
        PLOG_ERROR("register write event for PChannel:%s fail!!\n", ipport_.c_str());
        return false;
    }  

    return true;
}

void PChannel::dispatchMsg()
{
    for (auto msg : readQueue_) {
        if (!msgRouter_) {
            PLOG_ERROR("msgRouter is NOT set, drop msg!!\n");
            delete msg;
            continue;
        }
        msgRouter_->routMsg(msg, getPtr());
    }
    readQueue_.clear();
}

void PChannel::processReadEvent()
{
    int res = 0;
    checkin();
    
    char dataBuf[PSocket::PSOCKET_READ_UNIT_SIZE + PCodec::PCODEC_SLICE_BUF_SIZE];
    
    res = PSocket::readData(fd(), dataBuf, PSocket::PSOCKET_READ_UNIT_SIZE); 
    if (res == 0) {
        PLOG_WARN("channel of %s is closed !!\n", ipport_.c_str());
        close();
        return;
    } else if (res < 0) {
        if (errno == EAGAIN) {
            PLOG_DEBUG("resource of channel %s is temporary unavailable, fd:%d !!\n", ipport_.c_str(), fd());
        } else {
            PLOG_ERROR("channel %s READ socket failed, fd:%d, errno:%d !!\n", ipport_.c_str(), fd(), errno);
        }
        return;
    }
    codec_.collectMsg(dataBuf, res, &readQueue_);  

    dispatchMsg();
}

void PChannel::processWriteEvent()
{
    int res = 0;
    std::string *packet = NULL;
    size_t sentSize = 0;
    
    checkin();

    while (writeQueue_.size() > 0) {
        packet = writeQueue_.front();
        sentSize = 0;
        assert(packet);
        assert(packet->size() >= legacySendingPacketSize_);
        if (legacySendingPacketSize_ > 0) {
            //we have a legacy packet that not be sent out completely.
            sentSize = packet->size() - legacySendingPacketSize_;
        }
       
        while (sentSize < packet->size()) {
            res = PSocket::writeData(fd(), packet->data() + sentSize, packet->size() - sentSize);
            
            if (res == 0) {
                PLOG_WARN("channel of %s is closed !!\n", ipport_.c_str());
                close();
                return;
            } else if (res < 0) {
                if (errno == EAGAIN) {
                    PLOG_DEBUG("resource of channel %s is temporary unavailable, fd:%d !!\n", ipport_.c_str(), fd());
                    legacySendingPacketSize_ = packet->size() - sentSize;
                } else {
                    PLOG_ERROR("channel %s WRITE socket failed, fd:%d, errno:%d !!\n", ipport_.c_str(), fd(), errno);
                    legacySendingPacketSize_ = 0;
                }
                return;
            }
            sentSize += res;
        }
        
        legacySendingPacketSize_ = 0;
        writeQueue_.pop_front();
    };

    if (!loop_->delFdEvent(this, PEventOwner::PPOLL_WRITEABLE)) {
        PLOG_ERROR("remove write event of PChannel:%s fail!!", ipport_.c_str());
    }  
}

//should only be used by a client.
bool PChannel::reconnect()
{
    if (!autoConnect_) {
        PLOG_DEBUG("auto reconnection of %s is configured.\n", ipport_.c_str());
        return false;
    }

    int fd = PSocket::create(AF_INET);
    if (! PSocket::connect(fd, &addr_)) {
        PLOG_ERROR("PChannel reconnecting fail, old fd:%d, %s !!\n",  fd, ipport_.c_str());
        return false;
    }

    setupSocket(loop_, fd);
    if (!loop_->addFdEvent(this, PEventOwner::PPOLL_READABLE)) {
        PLOG_ERROR("Failed to setup PChannel event for for %s !!\n", ipport_.c_str());
        return false;
    }

    state_ = PCH_CONNECTED; 
    PLOG_INFO("PChannel for %s is reconnected successfully, fd:%d\n", ipport_.c_str(), fd);  
    return true;
}

}

