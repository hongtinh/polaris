//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PSOCKET_H___
#define __POLARIS_PSOCKET_H___

#include <netinet/in.h>
#include <string>

namespace polaris{

class PSocket{
  
public:
    PSocket() {}
    ~PSocket() {}
    static int create(int domain);
    static void close(int fd);
    static void listen(int fd, int port);
    static bool connect(int fd, const char *ipAddr, int port);
    static bool connect(int fd, const struct sockaddr_in* addr);
    static int accept(int fd, struct sockaddr_in* addr);
    static std::string getIpPort(struct sockaddr_in* addr);
    static bool setTcpNoDelay(int fd, bool on);
    static bool setReuseAddr(int fd, bool on);
    static bool setNonBlock(int fd, bool on);
    static bool setKeepAlive(int fd, bool on, int interval);
    static struct sockaddr_in getLocalAddr(int fd);
    static struct sockaddr_in getPeerAddr(int fd);
    static int readData(int fd, char *buf, size_t size);
    static int writeData(int fd, const char *buf, size_t size);

    enum {
        PSOCKET_READ_UNIT_SIZE = 1024*32 //32K
    };
    
private:
};


}

#endif
