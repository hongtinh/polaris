//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <functional>
#include "PScreener.h"
#include "PChannel.h"
#include "PTimer.h"
#include "../utility/PLog.h"

namespace polaris {

PScreener::PScreener(PEventLoop *loop)
    :loop_(loop),
	 size_(0),
	 buckets_(NULL),
	 channelMaxIdleSecs_(0),
     timer_(NULL),
	 tailIndex_(0),
	 headIndex_(0)
{
}

PScreener::~PScreener()
{
    if (timer_) delete timer_;
    //for (int i = 0; i < size_; i++) {
    //    kickout(i);
    //}
    delete[] buckets_;
}

bool PScreener::start(int maxIdleSecs)
{
    //bigger than 1 hour is meaningless ?
    if (maxIdleSecs > 3600) {
        PLOG_WARN("PScreener maxIdleSecs:%d, bigger than 3600 secs! reset it to be 3600.\n",  maxIdleSecs);
        channelMaxIdleSecs_ = 3600;
    } else {
        channelMaxIdleSecs_ = maxIdleSecs;
    }

    if (channelMaxIdleSecs_ <= 0) {
        PLOG_INFO("PScreener channelMaxIdleTime: infinite, (value <=0): %d\n",  channelMaxIdleSecs_);
        return true;
    }

    size_ = channelMaxIdleSecs_ + 1;
    buckets_ = new std::map<int, std::shared_ptr<PChannel>>[size_];
    tailIndex_ = 0;
    headIndex_ = size_ - 1;

    timer_ = new PTimer(loop_);
    timer_->setCallback(std::bind(&PScreener::screen, this, std::placeholders::_1));  

    timer_->start(1000, 1000);
    return true;
}


size_t PScreener::totalChannels() 
{
    size_t total = 0;
    for (int i = 0; i < size_; i++) {
        total += buckets_[i].size();
    }
    return total;
}

void PScreener::manage(std::shared_ptr<PChannel> ch)
{
    //just store the channel with arrage operation.
    arrange(ch);
}

void PScreener::arrange(std::shared_ptr<PChannel> ch)
{
    int fd = ch->fd();
    int lastIndex = ch->screenBucketIndex();

    if (lastIndex == headIndex_) {
        return;
    } else if (lastIndex >=0 && lastIndex < size_) { 
        auto it = buckets_[lastIndex].find(fd);
        if (it != buckets_[lastIndex].end()) {
            buckets_[lastIndex].erase(it);
        }
    }

    buckets_[headIndex_][fd] = ch;
    ch->setScreenBucketIndex(headIndex_);
    //PLOG_DEBUG("PScreener arrange ch:%d to bucket:%d. \n", ch->fd(), headIndex_);
}

void PScreener::tick()
{
    headIndex_++;
    tailIndex_++;
    headIndex_ %= size_;
    tailIndex_ %= size_;
}

void PScreener::kickout(int index)
{
    std::shared_ptr<PChannel> ch;
    auto it = buckets_[index].begin();
    while (it != buckets_[index].end()) {
        ch = it->second;
        //channel is referenced by (ch) and (it->second).
        if (ch.use_count() == 2 && ch->isScreened()) {
            PLOG_DEBUG("PScreener cleans the channel:%d, current bucket:%d, head bucket:%d, tail bucket:%d\n", 
                    ch->fd(), index, headIndex_, tailIndex_);
            it = buckets_[index].erase(it);
        } else {
            it++;
        }
    }
}

void PScreener::screen(PTimer *timer)
{
    tick();
    kickout(tailIndex_);
}

}


