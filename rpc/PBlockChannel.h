//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PBLOCKCHANNEL_H__
#define __POLARIS_PBLOCKCHANNEL_H__

#include <string>
#include <memory>
#include <deque>
#include "PMsg.h"
#include "PSocket.h"
#include "../utility/PCodec.h"

namespace polaris{

class PBlockChannel : public std::enable_shared_from_this<PBlockChannel> {

public:
    PBlockChannel(int fd);
    ~PBlockChannel();
    std::shared_ptr<PBlockChannel> getPtr() { return shared_from_this();}
    int state() { return state_; }  
    void setAutoReconnect(bool on) { autoConnect_ = on; }
    bool isAutoReconnect() { return autoConnect_; }
    bool sendMsg(PMsg *msg);
    bool recvMsg(std::deque<PMsg*> *queue);
    void close();
    int fd() { return fd_; }

private:
    void setupSocket(int fd);  
    bool reconnect();  

public:
    enum {
        PCH_DISCONNECTED = 0,        
        PCH_CONNECTED = 1,
    };
  
private:
    int fd_;
    struct sockaddr_in addr_;
    std::string ipport_;  
    PCodec codec_;
    int state_;
    bool autoConnect_;
};


}

#endif

