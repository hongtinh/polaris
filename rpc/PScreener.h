//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PSCREENER_H__
#define __POLARIS_PSCREENER_H__

#include <map>
#include <memory>

namespace polaris {

class PEventLoop;
class PTimer;
class PChannel;

//Time wheel implementation, maximum 1 second accuracy gap.
class PScreener {
public:
    PScreener(PEventLoop *loop);
    ~PScreener();
    //No copying
    PScreener(const PScreener &) = delete;
    PScreener& operator=(const PScreener &) = delete;

    bool start(int maxIdleSecs);
    void arrange(std::shared_ptr<PChannel> ch);
    void manage(std::shared_ptr<PChannel> ch);
    size_t totalChannels();

private:
    void tick();
    void kickout(int index);
    void screen(PTimer *timer);

private:
    PEventLoop *loop_;
    int size_;
    std::map<int, std::shared_ptr<PChannel>> *buckets_;
    int channelMaxIdleSecs_;
    PTimer *timer_;
    int tailIndex_;
    int headIndex_;
};

}

#endif
