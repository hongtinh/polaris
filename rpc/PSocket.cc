//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <stdio.h>
#include <assert.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/un.h>
#include <netinet/tcp.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <fcntl.h>
#include <netdb.h>
#include <errno.h>
#include <endian.h>
#include "PSocket.h"

namespace polaris{

const int PReadBufSize = 1024*32; //32K

int PSocket::create(int domain)
{
    int fd = ::socket(domain, SOCK_STREAM, 0);
    return fd;
}

void PSocket::close(int fd)
{
    ::close(fd);
}

void PSocket::listen(int fd, int port)
{
    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    if (::bind(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("bind");
        return;
    }

    if (::listen(fd, SOMAXCONN) == -1) {
        perror("listen");
        return;
    }
}

bool PSocket::connect(int fd, const char *ipAddr, int port)
{
    struct sockaddr_in addr;
    bzero(&addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr.s_addr = INADDR_ANY;

    if (!inet_aton(ipAddr, &addr.sin_addr)) {
        perror("inet_aton");
        return false;
    }

    if (::connect(fd, (struct sockaddr *)&addr, sizeof(addr)) == -1) {
        perror("connect");
        return false;
    }

    return true;
}

bool PSocket::connect(int fd, const struct sockaddr_in *addr)
{
    if (::connect(fd, (struct sockaddr *)(addr), sizeof (*addr)) == -1) {
        perror("connect");
        return false;
    }
    return true;
}


int PSocket::accept(int fd, struct sockaddr_in *addr)
{
    socklen_t len = sizeof(*addr);
    int afd = ::accept(fd, (struct sockaddr *)addr, &len);
    if (afd < 0) {
        perror("accept");
        return -1;
    }

    return afd;
}

std::string PSocket::getIpPort(struct sockaddr_in *addr)
{
    char buf[64] = {0};
    std::string ipport;
    
    ::inet_ntop(AF_INET, &(addr->sin_addr), buf, 64);
    size_t end = ::strlen(buf);
    snprintf(buf+end, 64 - end, ":%u", be16toh(addr->sin_port));
    
    ipport.append(buf);

    return ipport;
}

bool PSocket::setTcpNoDelay(int fd, bool on)
{
    int val = on ? 1 : 0;
    if (::setsockopt(fd, IPPROTO_TCP, TCP_NODELAY, &val, sizeof(val)) == -1) {
        perror("setsockopt TCP_NODELAY");
        return false;
    }

    return true;
}

// import from Redis.
bool PSocket::setKeepAlive(int fd, bool on, int interval)
{
    int val = on ? 1 : 0;

    if (::setsockopt(fd, SOL_SOCKET, SO_KEEPALIVE, &val, sizeof(val)) == -1) {
        perror("setsockopt SO_KEEPALIVE");
        return false;
    }

    //Send first probe after interval.
    val = interval;
    if (::setsockopt(fd, IPPROTO_TCP, TCP_KEEPIDLE, &val, sizeof(val)) < 0) {
        perror("setsockopt TCP_KEEPIDLE");
        return false;
    }

    //Send next probes after the specified interval. Note that we set the
    //delay as interval / 3, as we send three probes before detecting
    //an error (see the next setsockopt call). 
    val = interval/3;
    if (val == 0) val = 1;
    if (::setsockopt(fd, IPPROTO_TCP, TCP_KEEPINTVL, &val, sizeof(val)) < 0) {
        perror("setsockopt TCP_KEEPINTVL");
        return false;
    }

    //Consider the socket in error state after three we send three ACK
    //probes without getting a reply. 
    val = 3;
    if (::setsockopt(fd, IPPROTO_TCP, TCP_KEEPCNT, &val, sizeof(val)) < 0) {
        perror("setsockopt TCP_KEEPCNT");
        return false;
    }

    return true;
}

bool PSocket::setReuseAddr(int fd, bool on)
{
    int val = on ? 1 : 0;

    if (::setsockopt(fd, SOL_SOCKET, SO_REUSEADDR, &val, sizeof(val)) < 0) {
        perror("setsockopt SO_REUSEADDR");
        return false;
    }
    return true;
}

bool PSocket::setNonBlock(int fd, bool on)
{
    int flags;
    if ((flags = ::fcntl(fd, F_GETFL)) == -1) {
        perror("fcntl(F_GETFL)");
        return false;
    }

    if (on) {
        flags |= O_NONBLOCK;
    } else {
        flags &= ~O_NONBLOCK;
    }

    if (::fcntl(fd, F_SETFL, flags) == -1) {
        perror("fcntl(F_SETFL,O_NONBLOCK)");
        return false;
    }

    return true;
}

struct sockaddr_in PSocket::getLocalAddr(int fd)
{
    struct sockaddr_in localaddr;
    bzero(&localaddr, sizeof localaddr);
    socklen_t addrlen = static_cast<socklen_t>(sizeof localaddr);
    if (::getsockname(fd, (struct sockaddr *)(&localaddr), &addrlen) < 0) {
        perror("getLocalAddr");
    }
    return localaddr;
}

struct sockaddr_in PSocket::getPeerAddr(int fd)
{
    struct sockaddr_in peeraddr;
    bzero(&peeraddr, sizeof peeraddr);
    socklen_t addrlen = static_cast<socklen_t>(sizeof peeraddr);
    if (::getpeername(fd, (struct sockaddr *)(&peeraddr), &addrlen) < 0) {
        perror("getPeerAddr");
    }
    return peeraddr;
}

int PSocket::readData(int fd, char *buf, size_t size)
{
    return ::recv(fd, buf, size, 0);
}

int PSocket::writeData(int fd, const char *buf, size_t size)
{
    return ::send(fd, buf, size, 0);
}

}

