//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#ifndef __POLARIS_PTIMER_H__
#define __POLARIS_PTIMER_H__

#include <functional>
#include "PEventOwner.h"
#include "PEventLoop.h"

namespace polaris {

class PTimer;

typedef std::function<void (PTimer*)> timeoutCb;

//A relative timer implementation using linux timerfd.
class PTimer : public PEventOwner {
public:
    PTimer(PEventLoop *loop) ;
    virtual ~PTimer();
    void setCallback(timeoutCb cb);
    bool start(int timeoutSecs, int interval);
    bool stop();
    bool restart();
    bool restart(int timeoutSecs, int interval);
    PEventLoop* loop() { return loop_; }
    int timeoutMSecs() { return timeoutMSecs_; }
    int intervalMSecs() { return intervalMSecs_; }

private:
    void processReadEvent();
    void processWriteEvent();
    bool setTime(int timeoutMSecs, int intervalMSec);

private:
    PEventLoop *loop_;
    timeoutCb cb_;  
    int timeoutMSecs_;
    int intervalMSecs_;
    bool started_;

};


}

#endif
