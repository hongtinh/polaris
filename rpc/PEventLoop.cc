//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "PEventOwner.h"
#include "PEventLoop.h"
#include "../utility/PLog.h"

namespace polaris{

PEventLoop::PEventLoop()
{
    stop_ = false;
    poller_ = new PEpoll();
}

PEventLoop::~PEventLoop()
{
    if (poller_) delete poller_;
}

bool PEventLoop::addFdEvent(PEventOwner *owner, int mask)
{
    if (! poller_->addEvent(owner, mask)) {
        PLOG_ERROR("%s fail,  poller add event failed!!\n",  __func__); 
        return false;
    }
    
    int currentMask = owner->pollMask();
    owner->updateMask(currentMask|mask);
    
    return true;
}

bool PEventLoop::delFdEvent(PEventOwner *owner, int mask)
{
    if (! poller_->delEvent(owner, mask)) {
        PLOG_ERROR("%s fail, poller delete event failed, fd:%d!!\n",  __func__, owner->fd()); 
        return false;
    }
    
    int currentMask = owner->pollMask();
    currentMask &= (~mask);
    owner->updateMask(currentMask);   
    
    return true;
}

int PEventLoop::processEvents()
{
    int processed = 0, num_events = 0;
    PEventOwner *owner = NULL;
    int mask;

    //10s timeout by default
    num_events = poller_->wait(10000);
    std::vector<struct epoll_event> events = poller_->getEvents();

    for (int i = 0; i < num_events; i++) {
        owner = static_cast<PEventOwner*>(events[i].data.ptr);
        mask = poller_->getMask(&(events[i]));

        if (owner->pollMask() & mask & PEventOwner::PPOLL_READABLE) {
            owner->processReadEvent();
        }

        if (owner->pollMask() & mask & PEventOwner::PPOLL_WRITEABLE) {
            owner->processWriteEvent();
        }

        processed++;
    }

    return processed;
}

void PEventLoop::run()
{
    stop_ = false;
    while (!stop_) {
        processEvents();
    }
}

void PEventLoop::stop()
{
    stop_ = true;
}

}


