//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include "PBlockChannel.h"
#include "PSocket.h"
#include "../utility/PLog.h"
#include <deque>

namespace polaris{

PBlockChannel::PBlockChannel(int fd):codec_("\r\n\r\n")
{
    setupSocket(fd);
    state_ = PCH_CONNECTED; 
    PLOG_INFO("PBlockChannel for %s is setup successfully, fd:%d\n", ipport_.c_str(), fd);  
}


PBlockChannel::~PBlockChannel()
{
    close();
}

void PBlockChannel::setupSocket(int fd)
{
    //set block mode.
    PSocket::setNonBlock(fd, false);
    PSocket::setKeepAlive(fd, true, 30);

    addr_ = PSocket::getPeerAddr(fd);    
    ipport_ = PSocket::getIpPort(static_cast<struct sockaddr_in*>(&addr_)); 
    fd_ = fd;
}

void PBlockChannel::close()
{
    if (PCH_DISCONNECTED == state_) return;
    PSocket::close(fd());  
    state_ = PCH_DISCONNECTED;
}

bool PBlockChannel::sendMsg(PMsg *msg)
{
    assert(msg);
    if (state_ != PCH_CONNECTED) {
        if (! reconnect()) {
            PLOG_ERROR("sendMsg fail, connection of %s is not in connected state !!\n", ipport_.c_str());
            return false;
        } 
    }

    std::string packet;
    codec_.encode(msg, &packet);
    size_t sentSize = 0;
    while (sentSize < packet.size()) {
            
        int res = PSocket::writeData(fd_, packet.data() + sentSize, packet.size() - sentSize);
        if (res == 0) {
            state_ = PCH_DISCONNECTED;
            PLOG_ERROR("sendMsg fail, connection of %s is closed !!\n", ipport_.c_str());
            return false;
        } else if (res < 0) {
            PLOG_ERROR("channel %s WRITE socket failed, fd:%d, errno:%d !!\n", ipport_.c_str(), fd_, errno);
            return false;
        }

        sentSize += res;
    }

    return true;
}

bool PBlockChannel::recvMsg(std::deque<PMsg*> *queue)
{
    int res = 0;
    char dataBuf[PSocket::PSOCKET_READ_UNIT_SIZE + PCodec::PCODEC_SLICE_BUF_SIZE];

    res = PSocket::readData(fd_, dataBuf, PSocket::PSOCKET_READ_UNIT_SIZE); 
    if (res == 0) {
        PLOG_ERROR("recvMsg fail, connection of %s is closed !!\n", ipport_.c_str());
        close();
        return false;
    } else if (res < 0) {
        PLOG_ERROR("channel %s READ socket failed, fd:%d, errno:%d !!\n", ipport_.c_str(), fd(), errno);
        return false;
    }
    
    codec_.collectMsg(dataBuf, res, queue);  
    return true;
}

//should only be used by a client.
bool PBlockChannel::reconnect()
{
    if (!autoConnect_) {
        PLOG_DEBUG("auto reconnection of %s is configured.\n", ipport_.c_str());
        return false;
    }

    int fd = PSocket::create(AF_INET);
    if (! PSocket::connect(fd, &addr_)) {
        PLOG_ERROR("PBlockChannel reconnecting fail, old fd:%d, %s !!\n",  fd, ipport_.c_str());
        return false;
    }

    setupSocket(fd);
    state_ = PCH_CONNECTED; 
    PLOG_INFO("PBlockChannel for %s is reconnected successfully, fd:%d\n", ipport_.c_str(), fd);  
    return true;
}

}

