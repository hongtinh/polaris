//Copyright (C) Hao Hongting <haohongting@live.com>. All rights reserved.
//
//Redistribution and use in source and binary forms, with or without
//modification, are permitted provided that the following conditions 
//are met:
//
//   * Redistributions of source code must retain the above copyright 
//   notice, this list of conditions and the following disclaimer.
//   * Redistributions in binary form must reproduce the above copyright 
//   notice, this list of conditions and the following disclaimer in the 
//   documentation and/or other materials provided with the distribution.
//   * Neither the name of Hao Hongting nor the names of its contributors 
//   may be used to endorse or promote products derived from this software 
//   without specific prior written permission.
//
//THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
//"AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
//LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
//A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
//OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
//SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
//LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
//DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
//THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
//(INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
//OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

#include <unistd.h>
#include <sys/timerfd.h>
#include "PEventLoop.h"
#include "PTimer.h"
#include "../utility/PLog.h"

namespace polaris{


PTimer::PTimer(PEventLoop *loop)
{
    int fd = timerfd_create(CLOCK_MONOTONIC, TFD_NONBLOCK|TFD_CLOEXEC);
    assert(fd >= 0);
    loop_ = loop;
    setFd(fd);
    cb_ = NULL;
    timeoutMSecs_ = 0;
    intervalMSecs_ = 0;
    started_ = false;
}

PTimer::~PTimer()
{
    stop();
    ::close(fd());
}

void PTimer::setCallback(timeoutCb cb)
{
    assert(cb);
    cb_ = cb;
}

void PTimer::processReadEvent()
{
    uint64_t exp;
    ::read(fd(), &exp, sizeof(uint64_t));
    //TODO, could expire more than 1 times.
    if (cb_) {
        cb_(this);  
    } else {
        PLOG_ERROR("timer:%d callback function is NULL!!\n", fd());
    }
}

void PTimer::processWriteEvent()
{
    //do nothing.
}


bool PTimer::setTime(int timeoutMSecs, int intervalMSec)
{
    struct itimerspec val;  
    val.it_value.tv_sec = timeoutMSecs/1000;  
    val.it_value.tv_nsec = (timeoutMSecs%1000)*1000000;
    val.it_interval.tv_sec = intervalMSec/1000;
    val.it_interval.tv_nsec = (intervalMSec%1000)*1000000;  
    if (timerfd_settime(fd(), 0, &val, NULL) == -1) {
        PLOG_ERROR("timerfd_settime failed!!\n");
        return false;
    }

    return true;
}

bool PTimer::start(int timeoutMSecs, int intervalMSec)
{
    if (started_) {
        PLOG_TRACE("%s, PTimer %d already started!!\n", __func__, fd());
        return true;
    }

    if (! setTime(timeoutMSecs, intervalMSec)) return false;

    if (! loop_->addFdEvent(this, PEventOwner::PPOLL_READABLE)) {
        PLOG_ERROR("PTimer setup event failed!!\n");
        return false;
    }

    timeoutMSecs_ = timeoutMSecs;
    intervalMSecs_ = intervalMSec;
    started_ = true;
    PLOG_INFO("PTimer[%d] starts, will timeout after %d msecs, interval:%d msecs.\n", fd(), timeoutMSecs_, intervalMSecs_);
    return true;
}

bool PTimer::stop()
{
    if (!started_) {
        PLOG_TRACE("%s, PTimer %d already stopped !!\n", __func__, fd());
        return true;
    }

    if (! loop_->delFdEvent(this, PEventOwner::PPOLL_READABLE |PEventOwner::PPOLL_WRITEABLE)) {
        PLOG_ERROR("PTimer remove event failed!!\n");
        return false;
    }

    if (! setTime(0, 0)) return false;

    started_ = false;
    PLOG_INFO("PTimer:%d stop\n",fd());
    return true;
}

//should del&add poll event.
bool PTimer::restart()
{
    if (!started_) {
        PLOG_ERROR("%s, PTimer %d is not started !!\n", __func__, fd());
        return false;
    }

    if (! stop()) return false;
    if (! start(timeoutMSecs_, intervalMSecs_)) return false;

    return true;
}

bool PTimer::restart(int timeoutMSecs, int intervalMSec)
{
    timeoutMSecs_ = timeoutMSecs;
    intervalMSecs_ = intervalMSec;

    return restart();
}


}

